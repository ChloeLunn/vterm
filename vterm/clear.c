/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "screen.h"
#include "termio.h"
#include "termopts.h"
#include "vt100_font.h"

#include <ctype.h>
#include <stdlib.h>

extern struct terminal term_context;

// Clear the screen
void clear()
{
    stop_xmit(1);
    for (int y = 0; y < term_context.rows; y++)
    {
        for (int x = 0; x < term_context.columns; x++)
        {
            charbuf[y * x] = 0;
        }
    }
    screen_clear();
    cursor_home();
}

// reset the screen/font settings
void reset()
{
    term_context.attrib.dblsize = 0;
    term_context.attrib.fgcolour = FRONT_COLOUR_DEFAULT;
    term_context.attrib.bgcolour = BACK_COLOUR_DEFAULT;
    term_context.graphical = 0;
}

// clear and then reprint the screen
void reprint_screen()
{
    // Assert RTS to stop sending while we scroll
    stop_xmit(1);

    screen_clear();

    int t_x = term_context.col_pos;
    int t_y = term_context.row_pos;

    cursor_home();

    // foreach line
    for (term_context.row_pos = 0; term_context.row_pos < term_context.rows; term_context.row_pos++)
    {
        // foreach character
        for (term_context.col_pos = 0; term_context.col_pos < term_context.columns; term_context.col_pos++)
        {
            set_cursor();  // set the cursor to that line

            // if there is a valid character
            if (isprint(charbuf[term_context.row_pos * term_context.col_pos]))
            {
                tft_print_vt(charbuf[term_context.row_pos * term_context.col_pos], colours[term_context.attrib.fgcolour], colours[term_context.attrib.bgcolour]);
            }
            // assume non-print is end of line
            else
            {
                // stop and go to next line
                term_context.col_pos = 0;
                break;
            }
        }
    }

    // set cursor back to previous position
    term_context.row_pos = t_y;
    term_context.col_pos = t_x;
    set_cursor();
}

// reset the screen/font settings
void clear_reset_all()
{
    xon = 1;
    term_context.col_pos = 0;
    term_context.row_pos = 0;
    term_context.saved_pos[0] = 0;
    term_context.saved_pos[1] = 0;
    term_context.vt52 = 0;

    reset();
    clear();

    // reset tabstops
    for (term_context.col_pos = 0; term_context.col_pos < term_context.columns; term_context.col_pos++)
        tabstop[term_context.col_pos] = 0;  // clear

    // set to default
    for (term_context.col_pos = 0; term_context.col_pos <= term_context.columns - 1; term_context.col_pos += term_context.tabwidth)
        tabstop[term_context.col_pos] = 0xFF;

    tabstop[term_context.columns - 1] = 1;  // last column is atabstop

    cursor_home();

    do_bell();
}

// Clear to end of line, leaves cursor where it is
void clear_to_end_line()
{
    stop_xmit(1);

    set_cursor();

    // clear display data
    for (int x = term_context.col_pos; x < term_context.columns; x++)
    {
        attrib[term_context.row_pos * x] = 0;
        charbuf[term_context.row_pos * x] = 0;
    }

    // Graphically clear rest of line
    screen_clear_section(char_curs_x, char_curs_y - CHEIGHT, WIDTH, char_curs_y);

    set_cursor();
}

// Clear to end of screen, leaves cursor where it is
void clear_to_end_screen()
{
    stop_xmit(1);

    // clear end of line
    clear_to_end_line();

    // clear display data after this line
    for (int y = term_context.row_pos + 1; y < term_context.rows; y++)
    {
        for (int x = 0; x < term_context.columns; x++)
        {
            attrib[y * x] = 0;
            charbuf[y * x] = 0;
        }
    }

    // Graphically clear rest of screen
    screen_clear_section(0, char_curs_y, WIDTH, HEIGHT);

    set_cursor();
}

// Clear to end of line, leaves cursor where it is
void clear_to_start_line()
{
    stop_xmit(1);

    set_cursor();

    // clear display data
    for (int x = 0; x <= term_context.col_pos; x++)
    {
        attrib[term_context.row_pos * x] = 0;
        charbuf[term_context.row_pos * x] = 0;
    }

    // Graphically clear rest of line
    screen_clear_section(0, char_curs_y - CHEIGHT, char_curs_x, char_curs_y);

    set_cursor();
}

// Clear to end of screen, leaves cursor where it is
void clear_to_start_screen()
{
    stop_xmit(1);

    // clear in this line
    clear_to_start_line();

    // clear display data on previous lines
    for (int y = 0; y <= term_context.row_pos; y++)
    {
        for (int x = 0; x < term_context.columns; x++)
        {
            attrib[y * x] = 0;
            charbuf[y * x] = 0;
        }
    }

    // Graphically clear start of screen
    screen_clear_section(0, 0, WIDTH, char_curs_y - CHEIGHT - CLINE);
}
