/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#define BONUS_FRONT_COLOUR graphics::ORANGE

#define FRONT_COLOUR_DEFAULT 8
#define BACK_COLOUR_DEFAULT  0

// ~~~~ CHARACTERS

// Character width in pixels
#define CWIDTH (8)
// Character height in pixels
#define CHEIGHT (9)
// Line gap height in pixels
#define CLINE (1)
// Kerning gap width in pixels
#define CKERN (2)

// Cursor X to character print X
#define char_curs_x ((curs_x * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define char_curs_y ((curs_y * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

// Cursor X to character print X
#define max_char_curs_x (((TERMWIDTH - 1) * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define max_char_curs_y (((TERMHEIGHT - 1) * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

// ~~~~ FULL DISPLAY INFO

// pixels width without border
#define FBWIDTH (TERMWIDTH * (CWIDTH + CKERN))
// pixels height without border
#define FBHEIGHT (TERMHEIGHT * (CHEIGHT + CLINE))

// Border (top left align) in pixels
#define BORDER_X (20)
#define BORDER_Y (20)  // shift of the character height is already included

// Cursor X to character print X
#define WIDTH (((TERMWIDTH) * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define HEIGHT (((TERMHEIGHT) * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

#define BLACK       0
#define RED         1
#define GREEN       2
#define YELLOW      3
#define BLUE        4
#define MAGENTA     5
#define CYAN        6
#define WHITE       7
#define MAX_COLOURS 8

#define AMBER  9
#define ORANGE 10
#define GREY   11
#define PINK   12
#define PURPLE 13

/* RGB888 colours in ANSII colour order */
const static unsigned int colours[] = {
  0x000000, /* black */
  0xFF0000, /* red */
  0x00FF00, /* green */
  0xFFFF00, /* yellow */
  0x0000FF, /* blue */
  0xFF00FF, /* magenta */
  0x00FFFF, /* cyan */
  0xFFFFFF, /* white */
  0,
  0xf4bd14, /* amber */
  0xf46814, /* orange */
  0x999999, /* grey */
  0xff8c8c, /* pink */
  0x7400e0, /* purple */
  0xc3e000, /* lime */
};

const static unsigned int fg_colour = 0x000000;
const static unsigned int bg_colour = 0x000F00;

struct screen_info {
    int x_res;
    int y_res;
    int colour_res;
};

int screen_getset(struct screen_info* get, struct screen_info* set);
int screen_initialise();
int screen_sync();
int screen_clear();
int screen_clear_section(int x_s, int y_s, int x_f, int y_f);
int screen_scroll_lines(int by);
