/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "termopts.h"

#include <ctype.h>
#include <stdlib.h>

extern struct terminal term_context;

// process and ansi CSI command (e.g \033[31m to make text red; ONLY UNCLUDE NUMBER AND END LETTER)
void do_csi(const char* str, int len)
{
    char* cmd = (char*)str;

    int isDEC = cmd[0] == '?';
    if (isDEC)
        cmd++;

        /* Special DEC controls
         *  ESC [ ? 1 5 n     request printer status
         *  ESC [ ? 1 0 n     report - printer ready
         *  ESC [ ? 6 h       turn on region - origin mode
         *  ESC [ ? 6 l       turn off region - full screen mode
         *  ESC [ ? 1 h       cursor keys in applications mode
         *  ESC [ ? 1 l       cursor keys in cursor positioning mode
         *  ESC [ ? 5 h       black characters on white screen mode
         *  ESC [ ? 5 l       white characters on black screen mode
         *  ESC [ ? 7 h       auto wrap to new line
         *  ESC [ ? 7 l       auto wrap off
         *  ESC [ ? 8 h       keyboard auto repeat mode on
         *  ESC [ ? 8 l       keyboard auto repeat mode off
         *  ESC [ ? 9 h       480 scan line mode
         *  ESC [ ? 9 l       240 scan line mode
         *  ESC [ ? 1 8 h     print form feed on
         *  ESC [ ? 1 8 l     print form feed off
         *  ESC [ ? 1 9 h     print whole screen
         *  ESC [ ? 1 9 l     print only scroll region
         *  ESC [ 2 0 h       newline mode (LF, FF, VT, CR = CR/LF)  -- no '?' ???
         *  ESC [ 2 0 l       line feed mode (LF, FF, VT = LF ; CR = CR) -- no '?' ???
         */

#define a_val (params[0])
#define b_val (params[1])
#define c_val (params[2])

    int params[5] = {0, 0, 0, 0, 0};

    // load possible params
    for (int idx = 0; idx < 5; idx++)
    {
        // load the value
        params[idx] = atoi(cmd);

        // scrobble cmd to next ';'
        for (; cmd; cmd++)
        {
            // if ';' then add one and continue to next atoi
            if (cmd[0] == ';')
            {
                cmd++;
                break;
            }

            // if end, then break both loops
            if (cmd[0] == 0 || isalpha(cmd[0]))
            {
                idx = 6;
                break;
            }
        }
    }

    char c = str[len - 1];  // str NOT cmd because that way we can use len without checking isDEC

    // Serial.printf("\r\nAnsi code: \\033[%i;%i;%i;%i;%i%c\r\n", params[0], params[1], params[2], params[3], params[4], c);

    switch (c)
    {
        // UPPERCASE CONTROLS

    case 'A':  // cursor up
        if (a_val == 0)
            a_val = 1;
        cursor_up(a_val);
        return;
    case 'B':  // cursor down
        if (a_val == 0)
            a_val = 1;
        cursor_down(a_val);
        return;
    case 'C':  // cursor right
        if (a_val == 0)
            a_val = 1;
        cursor_right(a_val);
        return;
    case 'D':  // cursor left
        if (a_val == 0)
            a_val = 1;
        cursor_left(a_val);
        return;
    case 'E':  // move cursor to beginning of next line
        if (a_val == 0)
            a_val = 1;
        cursor_down(a_val);
        term_context.col_pos = 0;
        set_cursor();
        return;
    case 'F':  // move cursor to beginning of previous line
        if (a_val == 0)
            a_val = 1;
        cursor_up(a_val);
        term_context.col_pos = 0;
        set_cursor();
        return;
    case 'G':  // move cursor to column
        if (a_val == 0)
            a_val = 1;
        term_context.col_pos = a_val - 1;
        set_cursor();
        return;
    case 'H':  // cursor position
        if (a_val == 0)
            a_val = 1;
        if (b_val == 0)
            b_val = 1;
        if (b_val >= 1 && a_val >= 1)
        {
            term_context.col_pos = b_val - 1;
            term_context.row_pos = a_val - 1;
        }
        set_cursor();
        return;
    case 'J':  // Erase in display - cursor does not change
        {
            if (a_val == 0)  // clear to end of screen
                clear_to_end_screen();
            else if (a_val == 1)  // clear from beginning of screen
                clear_to_start_screen();
            else if (a_val == 2)  // clear all (move cursor top left)
                clear();
            else if (a_val == 3)  // clear all and delete scroll back (which we don't have anyway)
                clear();

            set_cursor();
        }
        return;
    case 'K':  // Erase in line - cursor does not change
        {
            if (a_val == 0)  // clear to end of line
                clear_to_end_line();
            else if (a_val == 1)  // clear from beginning of line
                clear_to_start_line();
            else if (a_val == 2)  // clear all (move cursor to left)
            {
                term_context.col_pos = 0;
                clear_to_end_line();
            }

            set_cursor();
        }
        return;
    case 'S':  // Scroll term up by n lines
        if (a_val == 0)
            a_val = 1;
        scroll_term_by(a_val);
        return;
    case 'T':  // Scroll term down by n (not implemented)
        return;

        // LOWERCASE CONTROLS

    case 'c':  // request terminal model identifier
        term_print(term_context.identity);
        return;
    case 'f':  // cursor position (alt)
        if (a_val == 0)
            a_val = 1;
        if (b_val == 0)
            b_val = 1;
        if (b_val >= 1 && a_val >= 1)
        {
            term_context.col_pos = b_val - 1;
            term_context.row_pos = a_val - 1;
            set_cursor();
        }
        return;
    case 'h':
        {
            if (a_val == 0)
                a_val = 1;
            if (isDEC)
            {
                switch (a_val)
                {
                case 2:  // switch from VT52 mode
                    term_context.vt52 = 0;
                    break;
                case 4:  // smooth scroll
                case 3:  // switch to 132 columns
                default:
                    break;
                }
            }
        }
        return;
    case 'i':  // change aux port status
        return;
    case 'l':
        {
            if (a_val == 0)
                a_val = 1;
            if (isDEC)
            {
                switch (a_val)
                {
                case 2:  // switch to VT52 mode
                    term_context.vt52 = 1;
                    break;
                case 4:  // jump scroll
                case 3:  // switch to 80 columns
                default:
                    break;
                }
            }
        }
        return;
    case 'm':  // Change style of characters
        {
            for (int i = 0; i < 5; i++)
            {
                switch (params[i])
                {
                case 0:
                    reset();
                    break;
                case 3:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.inverted = 1;
                    break;
                case 23:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.inverted = 0;
                    break;
                case 7:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.inverted = 1;
                    break;
                case 27:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.inverted = 0;
                    break;

                case 30:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 0;  // black
                    break;
                case 31:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 1;  // red
                    break;
                case 32:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 2;  // green
                    break;
                case 33:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 3;  // yellow
                    break;
                case 34:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 4;  // blue
                    break;
                case 35:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 5;  // magenta
                    break;
                case 36:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 6;  // cyan
                    break;
                case 37:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 7;  // white
                    break;

                case 40:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 0;  // black
                    break;
                case 41:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 1;  // red
                    break;
                case 42:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 2;  // green
                    break;
                case 43:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 3;  // yellow
                    break;
                case 44:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 4;  // blue
                    break;
                case 45:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 5;  // magenta
                    break;
                case 46:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 6;  // cyan
                    break;
                case 47:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 7;  // white
                    break;

                case 90:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 0;  // black
                    break;
                case 91:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 1;  // red
                    break;
                case 92:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 2;  // green
                    break;
                case 93:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 3;  // yellow
                    break;
                case 94:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 4;  // blue
                    break;
                case 95:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 5;  // magenta
                    break;
                case 96:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 6;  // cyan
                    break;
                case 97:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 7;  // white
                    break;

                case 100:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 0;  // black
                    break;
                case 101:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 1;  // red
                    break;
                case 102:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 2;  // green
                    break;
                case 103:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 3;  // yellow
                    break;
                case 104:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 4;  // blue
                    break;
                case 105:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 5;  // magenta
                    break;
                case 106:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 6;  // cyan
                    break;
                case 107:
                    term_context.attrib.dirty = 1;
                    term_context.attrib.fgcolour = 7;  // white
                    break;

                default:
                    break;
                }
            }
        }
        return;
    case 'n':  // device status report
        if (a_val == 0)
            a_val = 1;
        if (a_val == 6)
        {
            // report cursor position
            term_print("\033[");
            term_print(term_context.row_pos + 1);
            term_print(';');
            term_print(term_context.col_pos + 1);
            term_print('R');
        }
        return;
    case 'q':
        {
            // do LEDs here
        }
        return;
    case 'r':  // set margins (ignore this)
        return;
    case 's':  // save cursor position
        term_context.saved_pos[0] = term_context.col_pos;
        term_context.saved_pos[1] = term_context.row_pos;
        return;
    case 'u':  // restore cursor position
        term_context.col_pos = term_context.saved_pos[0];
        term_context.row_pos = term_context.saved_pos[1];
        set_cursor();
        return;
    default:
        break;
    }

    return;
}

// process an ansi command
int do_ansi(const char* str, int len)
{
    // ANSI
    switch (str[0])
    {
    case 'E':  // New line
        new_line(1, 1, 0);
        return 1;
    case 'D':  // New Line, no return to 0
        new_line(0, 1, 0);
        return 1;
    case 'M':  // Reverse Index (move up a line, scroll backwards if at top)
        cursor_up(1);
        return 1;
    case 'c':  // Reset to initial state and clear
        clear_reset_all();
        return 1;
    case 'H':  // Set a term_context.tabstop at the current column
        tabstop[term_context.col_pos] = 0xFF;
        return 1;
    case '[':  // CSIs (very like VT52s)
        do_csi(&str[1], len - 1);
        return 1;
    case 'Z':                               // What Are You (same as <ESC>[c and <ESC>[0c), <ESC>Z is not recommended as VT52 overlap
        term_print(term_context.identity);  // ID as base model VT100
        return 1;
    default:  // If we don't know it, then leave
        break;
    }
    return 0;
}

// Process a vt52 command
int do_vt52(const char* str, int len)
{
    if (term_context.vt52)
    {
        char c = str[0];

        switch (c)
        {
        case 'A':  // Cursor Up
            cursor_up(1);
            return 1;
        case 'B':  // Cursor Down
            cursor_down(1);
            return 1;
        case 'C':  // Cursor Right
            cursor_right(1);
            return 1;
        case 'D':  // Cursor Left
            cursor_left(1);
            return 1;
        case 'E':  // Clear screen and place cursor top left (we cheat by doing actual clear)
            clear();
            return 1;
        case 'F':  // Enter Graphics Mode
            term_context.graphical = 1;
            return 1;
        case 'G':  // Exit Graphics Mode
            term_context.graphical = 0;
            return 1;
        case 'H':  // Move Cursor to home
            cursor_home();
            return 1;
        case 'I':          // Reverse Line Feed (insert line above, then go to it)
            cursor_up(1);  // non-standard, no backwards scroll and not insert
            return 1;
        case 'J':  // Erase to end of screen from current cursor, do not move cursor
            clear_to_end_screen();
            return 1;
        case 'K':  // Erase all characters from cursor to end of line, do not move cursor
            clear_to_end_line();
            return 1;
        case 'L':  // Insert a line (treat as line feed)
            new_line(term_context.cr_lf, 1, 0);
            return 1;
        case 'M':  // Remove a line (treat as delete current line and then carriage return)
            term_context.col_pos = 0;
            clear_to_end_line();
            return 1;
        case 'Y':  // Move cursor to a specified position "\033Y[r][c]"
            {
r_again:
                // read in the next values for use as cursors wait a bit, just to be super sure they're in the buffer
                int r = term_read() - 31;
                if (r < 0)
                    goto r_again;
c_again:
                int c = term_read() - 31;
                if (c < 0)
                    goto c_again;

                // if valid, set cursor
                if (c >= 1 && r >= 1)
                {
                    term_context.col_pos = c - 1;
                    term_context.row_pos = r - 1;
                    set_cursor();
                }
            }
            return 1;
        case 'Z':
            term_print("\033/Z");
            return 1;
        case '=':  // Alternate keypad mode - trapped but does nothing
            return 1;
        case '>':  // Exit alternate keypad mode - trapped but does nothing
            return 1;
        case '<':  // Enter ANSI mode, ignore VT52 codes
            term_context.vt52 = 0;
            return 1;
        default:
            break;
        }
    }
    return 0;
}

int isAnsi(char c)
{
    return c == '\\';  // terminating 'string terminator'
}

// is the character part of a VT52 escape sequence?
int isvt52(char c)
{
    if (term_context.vt52)
        return c == '#' || c == '=' || c == '>' || c == '<' || isalpha(c);
    return 0;
}

// process reading and processing ANSI/VT52 escape commands
void do_escaped()
{
    escape_sequence[0] = 0;
    escape_len = 0;
    char c = 0;

    // Read in the command
    while (term_available() > 0)
    {
        // ascii 7-bit characters only!
        c = term_read();

        if (c <= 0)
            continue;

        // if another escape is read, then quit without processing
        if (c == _ESC || escape_len >= 14)
            return;

        // else, add it to the string
        strncat(escape_sequence, &c, 1);
        escape_len++;

        /* allowed terminating characters */
        if (isalpha(c) || isvt52(c) || isAnsi(c))
        {
            break;
        }
    }

    // if we're allowing ANSI (or VT) commands and we have a command with a valid length
    if (escape_len >= 1)
    {
        // Check the first letter first
        if (do_vt52(escape_sequence, escape_len))
        {
            return;
        }

        if (do_ansi(escape_sequence, escape_len))
        {
            return;
        }
    }

    return;
}
