/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "screen.h"
#include "termio.h"
#include "termopts.h"
#include "vt100_font.h"

/* used to store the status of the terminal control part */
struct terminal term_context;

char* tabstop;          /* tab positions */
unsigned char* charbuf; /* character buffer */
attribute_t* attrib;    /* character attributes */

// transmit status
int xon = 1;

// Cursor flashing state
int cursor_state = 0;

// String used for processing escaped sequences
char escape_sequence[15] = {[0] = 0};
int escape_len = 0;

// assert rts and xon/xoff
void stop_xmit(int stop)
{
    if (stop && !term_context.xmit_stopped)
    {
        term_rts(0);
        term_write(DC3);  // XOFF
        term_context.xmit_stopped = 1;
    }
    else if (!stop && term_context.xmit_stopped)
    {
        term_rts(1);
        term_write(DC1);  // XON
        term_context.xmit_stopped = 0;
    }
}

// Check if we need to assert RTS, and do it
void check_rts_assert()
{
    // if we reached the end of the buffer, assert the "shut up for a minute" line
    if (term_available() >= RTS_ASSERT)
        stop_xmit(1);
    // wait until it's cleared down to low numbers
    else if (term_available() <= RTS_DEASSERT)
        stop_xmit(0);
}

// puts the character on the screen using the OG vt100 font rom file
void tft_print_vt(int c, int colour, int backcolour)
{
    // if (inverted_video)  // flip colour and backcolour
    // {
    //     char newbmp[CHEIGHT];
    //     for (int i = 0; i < CHEIGHT; i++)
    //     {
    //         newbmp[i] = ~font_bitmaps[c][i];
    //     }

    //     // fill space between chars with rectangle so that it's not blocks, but smooth invertion
    //     screen_draw_bitmap(char_curs_x - CKERN, char_curs_y - CHEIGHT, font_bitmaps[0x82], CKERN, CHEIGHT, colour, backcolour);
    //     // write out inverted character
    //     screen_draw_bitmap(char_curs_x, char_curs_y - CHEIGHT, (const unsigned char*)newbmp, CWIDTH, CHEIGHT, colour, backcolour);
    // }
    // else
    screen_draw_bitmap(char_curs_x, char_curs_y - CHEIGHT, vt100_font[c], CWIDTH, CHEIGHT, colour, backcolour);
}

// Print a character
int print_char(int c, int no_scroll)
{
    /* not within the regular printable range, so don't print */
    if (c < 32 || c > 126)
        return 0;

    set_cursor();

    /* If the character is not blank, then we need to clear the old character */
    if (charbuf[term_context.row_pos * term_context.col_pos] != 0)
    {
        tft_print_vt(charbuf[term_context.row_pos * term_context.col_pos], colours[term_context.attrib.bgcolour], colours[term_context.attrib.bgcolour]);
        set_cursor();
    }

    /* if the graphics aren't enabled, and lowercase is also disabled, then convert c to uppercase */
    if (!term_context.lcase && !term_context.graphical && c <= 'z' && c >= 'a')
    {
        charbuf[term_context.row_pos * term_context.col_pos] = toupper(c);
    }

    /* if the DEC graphics set is enabled, and the character is in the graphics range */
    else if (term_context.graphical && c <= '~' && c >= '_')
    {
        /* convert to graphics value */
        charbuf[term_context.row_pos * term_context.col_pos] = c + 128;
    }

    /* just save c without any fudging */
    else if (isprint(c))
    {
        charbuf[term_context.row_pos * term_context.col_pos] = c;
    }

    /* print the bitmap to the screen */
    tft_print_vt(charbuf[term_context.row_pos * term_context.col_pos], colours[term_context.attrib.fgcolour], colours[term_context.attrib.bgcolour]);

    /* increment cursor along one */
    term_context.col_pos++;

    /* if we've hit the width, and we are doing soft newlines, do a new line */
    if (term_context.autowrap && term_context.col_pos >= term_context.columns)
    {
        new_line(1, 1, no_scroll);
    }
    /* if we've hit the width, and we are NOT doing soft newlines, keep the cursor where it is */
    else if (!term_context.autowrap && term_context.col_pos >= term_context.columns)
    {
        term_context.col_pos--;
    }

    return 1;
}

// Scan arriving data to display
void receive_loop()
{
    // time to flash the cursor
    if (flash_throttle >= (term_context.blink / 2))
    {
        flash_cursor();
        flash_throttle = 0;
    }

    check_rts_assert();

    // while data is in the buffer, keep loading it without showing the cursor
    while (term_available() > 0)
    {
        check_rts_assert();

        if (cursor_state)
        {
            flash_cursor();
        }

        // check_rts_assert();
        int c = term_read();

        if (c <= 0)
        {
            return;
        }

        // If enabled, convert a DEL into BS
        if (c == DEL && term_context.del_bs)
        {
            c = BS;
        }

        // try to print it
        if (!print_char((char)c, 0))
        {
            // we can't, so handle it as a special character
            switch (c)
            {
            case DC3:  // XOFF
                xon = 0;
                break;
            case DC1:  // XON
                xon = 1;
                break;

            // Carriage return (CTRL+M), move cursor to start of line (or new line)
            case CR:
                new_line(1, term_context.cr_lf, 0);
                break;

            // Bell
            case BEL:
                {
                    term_rts(0);
                    switch (term_context.bell)
                    {
                    case BELL_BEEP:
                        term_beep(BELL_TONE, BELL_TIME);
                        break;

                    case BELL_LIGHT:
                        term_lamp(BELL_TONE, BELL_TIME);
                        break;

                    case BELL_SCREEN:
                        break;

                    case BELL_NONE:
                    default:
                        break;
                    }
                    term_rts(1);
                }
                break;

            // On a VT100 VT and FF inherit from LF:
            case VT:  // Vertical Tab (CTRL+K), move cursor down a line and to beginning
            case FF:  // Form feed (CTRL+L), move cursor down a line and to beginning
            case LF:  // Line feed (CTRL+J), move cursor down a line and to beginning
                new_line(term_context.cr_lf, 1, 0);
                break;

            // Horizontal Tab (CTRL+I), Move the cursor to the next tab stop, or to the right margin if no further tab stops are present on the line
            case HT:
                {
                    for (term_context.col_pos = term_context.col_pos + 1; term_context.col_pos < term_context.columns; term_context.col_pos++)
                    {
                        if (tabstop[term_context.col_pos])
                            break;
                    }
                    set_cursor();
                }
                break;

            // Backspace (CTRL+H), Move the cursor to the left one character position, unless it is at the left margin, in which case no action
            case BS:
                cursor_left(1);
                break;

            case DEL:  // Delete, Ignored on input (not stored in input buffer)
                break;

            case NUL:  // NULL (CTRL+@), Ignored on input (not stored in input buffer)
                break;

            case ESC:  // Escape (CTRL+[), ANSI control sequence (e.g. <ESC>[3~ for DEL)
                do_escaped();
                break;

            default:  // Any other character, do nothing
                break;
            }
        }
    }
}

int main(int argc, char** argv)
{
    screen_initialise();

    term_initialise();

    // clear and reset
    clear_reset_all();

    // disable transmit until we enter receive_loop
    term_context.xmit_stopped = 0; /* unstop, so the stop function works */
    stop_xmit(1);

    while (1)
    {
        receive_loop();
        screen_sync();
    }

    return 0;
}
