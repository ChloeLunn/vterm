/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#if !H_TERMOPTS
#define H_TERMOPTS 1

/* put this here temporarily to shut up the linter */
#ifndef __packed
#define __packed __attribute__((packed))
#endif

typedef union {
    struct {
        unsigned short dirty : 1;    /* any attribute is set */
        unsigned short inverted : 1; /* fg/bg is inverted */
        unsigned short dblwide : 1;  /* character is double width size */
        unsigned short dbltall : 1;  /* character is double height size */
        unsigned short fgset : 1;    /* forground is non-default */
        unsigned short fgcolour : 3; /* foreground colour 0-7 */
        unsigned short bgset : 1;    /* background is non-default */
        unsigned short bgcolour : 3; /* background colour 0-7 */
    } __packed;
    unsigned short val;
} __packed attribute_t;

struct terminal {
    /* io handling */
    int echo;    /* echo characters */
    int xon;     /* use XON/XOFF flow control */
    int speed;   /* baud rate */
    char cdelay; /* character delay transmit multiplier */

    /* boundary handling */
    int columns;  /* number of char columns on terminal */
    int rows;     /* number of char lines on terminal */
    int tabwidth; /* number of spaces to expand a tab to */
    int autowrap; /* do newline if we think we hit the last char in the row */
    int cr_lf;    /* newline mode (LF, FF, VT, CR = CR/LF) ELSE (LF, FF, VT = LF ; CR = CR)*/

    /* command handling */
    char identity[10]; /* identity check response string */
    int vt52;          /* run vt52 commands mode */
    int bell;          /* bell option 0-4 */
    int del_bs;        /* treat del as bs */

    /* display options */
    int blink;          /* blink time */
    int lcase;          /* allow lowercase */
    int cursor;         /* cursor option 0-6 */
    int graphical;      /* enable graphical character set */
    attribute_t attrib; /* current attributes */

    /* running status info */
    int col_pos;      /* current output column */
    int row_pos;      /* current output row */
    int saved_pos[2]; /* saved position x,y */
    int xmit_stopped; /* transmission disabled */
};

struct terminal default_term = {
  .lcase = 1,
  .echo = 0,
  .xon = 1,
  .speed = 9600,
  .cdelay = 0,
  .columns = 80,
  .rows = 24,
  .tabwidth = 8,
  .autowrap = 1,
  .cursor = 0,
  .blink = 1200,
  .identity = {'\033', '[', '?', '1', ';', '0', 'c', 0},
  .vt52 = 0,
  .bell = 0,
  .del_bs = 1,
  .graphical = 0,

  .col_pos = 0,
  .row_pos = 0,
  .saved_pos = {0, 0},
  .xmit_stopped = 1,
};

// ~~~~ TERMINAL SERIAL CONNECTION SETTINGS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Size of buffer to be full before asserting RTS
#define RTS_ASSERT (350)

// Size of buffer to be full before unasserting RTS
#define RTS_DEASSERT (0)

// ~~~~ TERMINAL BELL HANDLING

#define BELL_NONE   (0)
#define BELL_SCREEN (1)  // Flash screen on BEL
#define BELL_LIGHT  (2)  // Flash LED on BEL
#define BELL_BEEP   (3)  // Beep on BEL

#define BELL_TIME (20)
#define BELL_TONE (440)

// ~~~~ TERMINAL CHARACTER HANDLING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ASCII Control Characters
#define NUL  (0x00)
#define SOH  (0x01)
#define STX  (0x02)
#define ETX  (0x03)
#define EOT  (0x04)
#define ENQ  (0x05)
#define ACK  (0x06)
#define BEL  (0x07)
#define BS   (0x08)
#define HT   (0x09)
#define LF   (0x0A)
#define VT   (0x0B)
#define FF   (0x0C)
#define CR   (0x0D)
#define SO   (0x0E)
#define SI   (0x0F)
#define DLE  (0x10)
#define DC1  (0x11)
#define XON  (DC1)
#define DC2  (0x12)
#define DC3  (0x13)
#define XOFF (DC3)
#define DC4  (0x14)
#define NAK  (0x15)
#define SYN  (0x16)
#define ETB  (0x17)
#define CAN  (0x18)
#define EM   (0x19)
#define EOF  (0x1A)
#define SUB  (0x1A)
#define ESC  (0x1B)
#define FS   (0x1C)
#define GS   (0x1D)
#define RS   (0x1E)
#define US   (0x1F)
#define DEL  (0x7F)
#endif
