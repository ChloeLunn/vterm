/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "screen.h"
#include "termio.h"
#include "termopts.h"
#include "vt100_font.h"

#include <ctype.h>
#include <stdlib.h>

extern struct terminal term_context;

// Set the cursor to the current x,y position
void set_cursor()
{
    // if out of bounds, set it to be inside bounds
    if (term_context.col_pos >= term_context.columns)
        term_context.col_pos = term_context.columns - 1;

    if (term_context.row_pos >= term_context.rows)
        term_context.row_pos = term_context.rows - 1;

    screen_set_cursor(char_curs_x, char_curs_y);
}

// Move the cursor to home
void cursor_home()
{
    term_context.col_pos = 0;
    term_context.row_pos = 0;
    set_cursor();
}

// Flash the cursor
void flash_cursor()
{
    set_cursor();  // make sure the tft cursor matches our buffer cursor

    cursor_state = !cursor_state;

    if (cursor_state)
    {
        tft_print_vt(128 + term_context.cursor, colours[front_colour], colours[back_colour]);
    }
    else
    {
        /* undraw the cursor */
        tft_print_vt(128 + term_context.cursor, colours[back_colour], colours[back_colour]);

        /* if there's a character */
        if (charbuf[term_context.row_pos * term_context.col_pos] != 0)
        {
            /* put cursor back to the same place */
            set_cursor();

            /* now draw the char */
            tft_print_vt(charbuf[term_context.row_pos * term_context.col_pos], colours[front_colour], colours[back_colour]);
        }
    }

    set_cursor();  // make sure the tft cursor matches our buffer cursor
}

// Move the cursor left by num. stop if hit left edge
void cursor_left(int num)
{
    // term_printf("Moving left by %i\r\n", num);
    while (num > 0)
    {
        if (term_context.col_pos > 0)
            term_context.col_pos -= 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor right by num. stop if hit right edge
void cursor_right(int num)
{
    // term_printf("Moving right by %i\r\n", num);
    while (num > 0)
    {
        if (term_context.col_pos < term_context.columns - 1)
            term_context.col_pos += 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor up by num. stop if hit top edge
void cursor_up(int num)
{
    // term_printf("Moving up by %i\r\n", num);
    while (num > 0)
    {
        if (term_context.row_pos > 0)
            term_context.row_pos -= 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor down by num. stop if hit bottom edge
void cursor_down(int num)
{
    // term_printf("Moving down by %i\r\n", num);
    while (num > 0)
    {
        if (term_context.row_pos < term_context.rows - 1)
            term_context.row_pos += 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor to a new line. If b then move X to 0, optionally scroll when at end of terminal
void new_line(int cr, int lf, int no_scroll)
{
    if (cr)
    {
        term_context.col_pos = 0;  // go to beginning of line
    }

    if (lf)
    {
        term_context.row_pos++;  // increment line counter

        if (term_context.row_pos >= term_context.rows)  // if we've hit the end of the terminal
        {
            term_context.row_pos = 0;  // reset to line 1

            if (!no_scroll)
            {
                scroll_term_by(1);
            }
        }
    }

    set_cursor();  // set the cursor
}

// Scroll the terminal lines (more == more efficient)
void scroll_term_by(int by)
{
    // if we're scrolling by the whole screen, then just do a clear as it's faster
    if (term_context.rows == by)
    {
        // Assert RTS to stop sending while we scroll
        stop_xmit(1);
        clear();
        return;
    }

    // Assert RTS to stop sending while we scroll
    stop_xmit(1);

    // scroll display up lines graphically
    screen_scroll_lines(((CHEIGHT + CLINE) * by));

    // edit text buffer to reflect scrolling
    for (term_context.row_pos = 0; term_context.row_pos < term_context.rows; term_context.row_pos++)
    {
        // foreach character
        for (term_context.col_pos = 0; term_context.col_pos < term_context.columns; term_context.col_pos++)
        {
            // copy the scrolled line into the top
            if (term_context.row_pos + by < term_context.rows)
            {
                charbuf[term_context.row_pos * term_context.col_pos] = charbuf[(term_context.row_pos + by) * term_context.col_pos];

                // assume non-print is end of line
                if (!charbuf[term_context.row_pos * term_context.col_pos])
                {
                    // stop and go to next line
                    term_context.col_pos = 0;
                    break;
                }
            }
            // clear the lines at the end (possibly not needed)
            else
            {
                charbuf[term_context.row_pos * term_context.col_pos] = 0;
            }
        }
    }

    // set cursor to new position
    term_context.row_pos = term_context.rows - by;
    term_context.col_pos = 0;
    set_cursor();
}
