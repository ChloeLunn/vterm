vterm is a portable virtual terminal

The software has evolved from originally being used with a 3.5" 360x240 TFT and an AVR atmega328p, to a 7" 800x480 TFT with an ARM SAMD21G, to now where the current version will also run on top of the Linux framebuffer

It is designed to be a fairly lightweight implementation of a VT100 character and command handling, with a few extensions such as colours.
