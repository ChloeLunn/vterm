/*
Modified BSD License

Copyright (c) 2018-2023, Chloe Lunn <chloetlunn@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

4. Any modified sections of source code used in redistributions covered under
   these licensing terms must be provided freely and free of charge in source
   code form to any party which requests the modifications. Only the modified
   sections need to be made available if the redistribution is included in a
   larger body of work.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
    Bitmap font object - similar to elf, funnily enough
*/

#if !H_BFO
#define H_BFO 1

/* put this here temporarily to shut up the linter */
#ifndef __packed
#define __packed __attribute__((packed))
#endif

typedef struct bfo_header {
    unsigned char b_magic[4];
    unsigned char b_version; /* bfo version */
    unsigned char b_class;   /* character encoding class */
    unsigned char b_data;    /* bitmap encoding type */
    unsigned char b_flags;   /* flags*/
    unsigned int b_phnum;    /* number of pages */
} __packed Bfo_Bhdr;

static unsigned char bfo_magic[4] = {'B', 'F', 'O', '>'};

#define BFO_VERSION 1

#define BFO_CLASS_NONE  0
#define BFO_CLASS_ASCII 1 /* ascii character mapping */

#define BFO_ENCODE_1FG 1 /* 1-bit colour, high as foreground */
#define BFO_ENCODE_1BG 2 /* 1-bit colour, high as background */

/* code/display page header  */
typedef struct bfo_pheader {
    unsigned int p_type;   /* entry type */
    unsigned int p_offset; /* date offset in object file */
    unsigned int p_base;   /* character value offset */
    unsigned int p_linesz; /* line size (pixels per line bitmap) */
    unsigned int p_charsz; /* character size (lines per char bitmap) */
    unsigned int p_nchars; /* number of characters in page */
    unsigned int p_flags;  /* flags */
} __packed Bfo_Phdr;

#define PT_NULL  0
#define PT_BASE  1 /* base character bitmaps */
#define PT_GRAPH 2 /* graphical overlay page */

#endif
